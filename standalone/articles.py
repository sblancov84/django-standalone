import sys
import os
import django
from abc import ABC, abstractmethod


sys.path.append("mysite")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
django.setup()


from hello.models import Article


class Crud(ABC):
    @abstractmethod
    def create(self):
        pass

    @abstractmethod
    def read(self):
        pass

    @abstractmethod
    def delete(self):
        pass


class Articles(Crud):

    def create(self, name):
        Article.objects.create(name=name)

    def read(self):
        articles = Article.objects.all()
        return articles

    def delete(self, name):
        pk = Article.objects.filter(name=name).delete()
        return pk

    def exists(self, name):
        articles = Article.objects.filter(name=name)
        return len(articles) > 0
