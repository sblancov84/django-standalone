import sys
from articles import Articles


if __name__ == "__main__":
    try:
        name = sys.argv[1]
        articles = Articles()
        articles.create(name)
    except IndexError as e:
        print("ERROR: hey mate, I need an article name")

    print("Done")
