from behave import given, when, then

from standalone.articles import Articles


@given('I check "{name}" article does not exist or delete it')
def check_article_not_exist(context, name):
    articles = Articles()
    if articles.exists(name):
        articles.delete(name)
    assert not articles.exists(name)

@when('I create "{name}" article')
def create_new_article(context, name):
    articles = Articles()
    try:
        articles.create(name)
    except Exception as e:
        assert False, e
    assert True

@then('"{name}" article can be retrieved')
def check_article_exists(context, name):
    articles = Articles()
    assert articles.exists(name)

@then('I can delete "{name}" article')
def check_article_exists(context, name):
    articles = Articles()
    assert articles.exists(name)
