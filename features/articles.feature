Feature: Manage articles

  Scenario: Create a single new article
    Given I check "new" article does not exist or delete it
    When I create "new" article
    Then "new" article can be retrieved
    And I can delete "new" article

  Scenario: Retrieve all articles
    Given There is no article or I delete all of them
    And I create "squirtle" article
    And I create "charmander" article
    When I retrieve all articles
    Then I can check "squirtle" article is retrieved
    And I can check "charmander" article is retrieved
