.PHONY : standalone

run:
	python mysite/manage.py runserver

makemigrations:
	python mysite/manage.py makemigrations

migrate:
	python mysite/manage.py migrate

read-standalone:
	python standalone/read.py
