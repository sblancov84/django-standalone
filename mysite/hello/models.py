from django.db import models


class Article(models.Model):

    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "hello"
